package co.simplon.promo16.springbootjdbcthymeleaf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.springbootjdbcthymeleaf.model.Subscriber;

@Repository
public class SubscriberDAOImpl implements SubscriberDAO {

    private static final String GET_SUBSCRIBERS = "SELECT * FROM subscribers";
    private static final String ADD_SUBSCRIBER = "INSERT INTO subscribers(firstName, lastName, email, createdAt) VALUES(?,?,?)";

    @Autowired
    DataSource datasource;

    private Connection cnx;

    @Override
    public List<Subscriber> findAll() {
        List<Subscriber> subscriberList = new ArrayList<>();
        try {
            cnx = datasource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_SUBSCRIBERS);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Subscriber subscriber = new Subscriber(
                        rs.getInt("subscriberId"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("email"),
                        rs.getTimestamp("createdAt"));

                subscriberList.add(subscriber);

            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return subscriberList;
    }

    @Override
    public Subscriber add(Subscriber subscriber) {

        try {
            cnx = datasource.getConnection();
            PreparedStatement ps = cnx
                    .prepareStatement("INSERT INTO subscribers(firstName, lastName, email, createdAt");
            ps.setString(1, subscriber.getFirstName());
            ps.setString(2, subscriber.getLastName());
            ps.setString(3, subscriber.getEmail());
            ps.executeUpdate();
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subscriber;
    }

}
