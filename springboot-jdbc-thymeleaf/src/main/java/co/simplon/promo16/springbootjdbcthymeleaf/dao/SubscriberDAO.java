package co.simplon.promo16.springbootjdbcthymeleaf.dao;

import java.util.List;

import co.simplon.promo16.springbootjdbcthymeleaf.model.Subscriber;

public interface SubscriberDAO {
    
    public List<Subscriber> findAll();

    public Subscriber add(Subscriber subscriber);
}
