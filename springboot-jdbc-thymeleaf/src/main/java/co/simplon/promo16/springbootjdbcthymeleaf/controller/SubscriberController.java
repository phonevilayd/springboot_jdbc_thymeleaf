package co.simplon.promo16.springbootjdbcthymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.promo16.springbootjdbcthymeleaf.dao.SubscriberDAO;

@Controller
public class SubscriberController {
    @Autowired
    private SubscriberDAO subscriberDAO;

    @GetMapping("/")
    public String getAllSubscribers(Model model) {
        model.addAttribute("subscribers", subscriberDAO.findAll());
        return "index";
    }

}
